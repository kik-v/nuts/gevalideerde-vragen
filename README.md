# Gevalideerde Vraag - ValidatedQueryCredential
Hieronder staat een *voorbeeld* van de toepassing Gevalideerde Vraag Credential JSONLD.
```json
{
  "@context": "https://kik-v.nl/context/v1.json",
  "credentialSubject": {
    "id": "did:nuts:AXFwxMC5MD8rHeHcsEZUW6RLa67Gjen6vWAnc4mjUsCa",
    "validatedQuery": {
      "description": "TODO: Should be added to the header file",
      "identifier": "2c0d8bfa-6e04-402d-b2b4-f56229ad85b7",
      "name": "2.1.2. Aantal fte",
      "ontology": [],
      "profile": "https://kik-v-publicatieplatform.nl/documentatie/demo-bas/dev",
      "sparql": "# Indicator: ODB Personele samenstelling 2.1.2\n# Parameters: -\n# Ontologie: versie 2.0.0 of nieuwer\n\nPREFIX onz-g: <http://purl.org/ozo/onz-g#>\nPREFIX onz-pers: <http://purl.org/ozo/onz-pers#>\nPREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n\nSELECT \n    (SUM(?aantal) AS ?indicator) \n    ?eenheid\nWHERE \n{\n    {\n        SELECT DISTINCT \n            ?start_periode \n            ?eind_periode \n            ?gewerkte_periode \n            ?uren\n        WHERE \n        { \n            BIND (\"2022-01-01\"^^xsd:date AS ?start_periode)\n            BIND (\"2022-12-31\"^^xsd:date AS ?eind_periode)  \n            \n            # selecteer aantal gewerkte uren binnen periode die gekoppeld zijn aan werkovereenkomst \n            ?gewerkte_periode \n                a onz-pers:GewerktePeriode ;\n                onz-g:hasBeginTimeStamp ?start_gewerktDT ;\n                onz-g:hasEndTimeStamp ?eind_gewerktDT ;\n                onz-g:hasQuality / onz-g:hasQualityValue / onz-g:hasDataValue ?uren ;\n                onz-g:definedBy ?overeenkomst .\n            BIND(STRDT(SUBSTR(STR(?start_gewerktDT),1,10), xsd:date) AS ?start_gewerkt)\n            BIND(STRDT(SUBSTR(STR(?eind_gewerktDT),1,10), xsd:date) AS ?eind_gewerkt)\n    \t\t# selecteer gewerkte tijd in de gevraagde periode\n            FILTER(?start_gewerkt >= ?start_periode && ?eind_gewerkt <= ?eind_periode)\n\n            # selecteer werkovereenkomsten waarbij werknemer een zorgverlenerfunctie heeft\n            ?overeenkomst \n                a onz-pers:ArbeidsOvereenkomst ;\n                onz-g:isAbout/a onz-pers:ZorgverlenerFunctie .       \n        }\n    }\n    \n    ?e \n     \ta onz-g:UnitOfWorkload ;\n       \tonz-g:hasDataValue ?corr_factor ;\n       \tonz-pers:hasDenominatorQualityValue onz-g:Week ;\n       \trdfs:label ?eenheid .\n        \n    BIND ((360 * (YEAR(?eind_periode + \"P1D\"^^xsd:duration) - YEAR(?start_periode))) +\n    (30 * (MONTH(?eind_periode + \"P1D\"^^xsd:duration) - MONTH(?start_periode))) +\n    (DAY(?eind_periode + \"P1D\"^^xsd:duration) - DAY(?start_periode)) AS ?dagen_periode)\n    \n    BIND (?uren/(?dagen_periode/360*47)/?corr_factor AS ?aantal)\n} \nGROUP BY ?eenheid"
    }
  },
  "issuanceDate": "2023-06-24T23:01:12.580843054Z",
  "issuer": "did:nuts:AXFwxMC5MD8rHeHcsEZUW6RLa67Gjen6vWAnc4mjUsCa",
  "type": "ValidatedQueryCredential",
  "visibility": "public"
}
```
